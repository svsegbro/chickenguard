# Chickenguard

## Version 1
For the V1:
* Let's assume power is available. No batteries, solar panels, etc. in this version.
* Horizontal sliding door.
* Upcycle when possible:
  * An old car entenna?
  * The CD-rom drive of an old computer? 
* An Arduino Nano to trigger door opening/closing.

### References
* https://www.youtube.com/watch?v=bSUgofAG0oU
* A series:
  * Part 1: https://www.youtube.com/watch?v=n6OhY3Bnh7c
  * Part 2: https://www.youtube.com/watch?v=4G8VhygrajI
  * Part 3: https://www.youtube.com/watch?v=LyTHrtQqUX8

### Parts list
* Arduino Nano
  * https://store.arduino.cc/arduino-nano-every-with-headers
  * https://store.arduino.cc/nano-every-pack

## Version X
In future versions:
* Battery powered.

### Power consumption
* https://www.instructables.com/id/Small-Format-Arduino-Nano-With-USB-Charging-Circui/
* https://learn.adafruit.com/all-about-batteries/how-to-pick-the-right-battery-for-your-project
* https://www.instructables.com/id/Solar-Charged-Battery-Powered-Arduino-Uno/

